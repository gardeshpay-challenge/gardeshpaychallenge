import React from 'react';
import './App.css';
import Header from './components/header';
import Form from './components/formCreate';
import Section from './components/section';
import Add from './components/formAdd';
// import Footer from './components/footer';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showForm: false,
      data: {}
    };
    this.showForm = this.showForm.bind(this);
    this.hideForm = this.hideForm.bind(this);
    this.getData = this.getData.bind(this);
  }

  showForm = () => this.setState({showForm: true});

  hideForm = () => this.setState({showForm: false});

  getData = (state) => { 
    this.setState({ data: state });
}

  render() {
    return (
        <div className="container">
          <Header />
          <Form  onClick={this.showForm} />
          <Section data={this.state.data} />
          <Add show={this.state.showForm} onClick={this.hideForm}
          sendData={this.getData} />
        </div>
    );
  }
}

export default App;
