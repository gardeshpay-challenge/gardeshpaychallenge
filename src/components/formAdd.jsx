import React from "react";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import "./formAdd.css";

const validation = Yup.object().shape({
  name: Yup.string()
    .min(2, "too short")
    .max(50, "to long")
    .required("field is empty"),
  lastName: Yup.string()
    .min(2, "too short")
    .max(50, "to long")
    .required("field is empty"),
  mobile: Yup.string().matches(/\d{10}/g, 'not valid phone number'),
  email: Yup.string().email("Invalid email").required("Required"),
});

export default class Add extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      lastName: "",
      mobile: "",
      email: "",
    };
    this.hideForm = this.hideForm.bind(this);
    // this.handleChange = this.handleChange.bind(this);
    this.handleSub = this.handleSub.bind(this);
  }

  hideForm = () => this.props.onClick();

  // handleChange = (name, event) => {
  //   console.log(name, event.target.value);
  //   this.setState({
  //     [name]: event.target.value,
  //   });
  // };

  handleSub = (values, actions) => this.props.sendData(values);

  render() {
    return (
      <div
        className="add"
        style={{ display: this.props.show ? "flex" : " none" }}
      >
        <div className="add-container">
          <div className="logo-close" onClick={this.hideForm}>
            <i className="fa fa-close"></i>
          </div>

          <div className="inputs-add">
            <p>افزودن کاربر جدید</p>
            <div className="fields">
              <Formik
                validationSchema={validation}
                initialValues={{
                  name: "",
                  lastName: "",
                  mobile: "",
                  email: "",
                }}
                onSubmit={this.handleSub}
              >
               {({ errors, touched }) => (
                <Form className="form-on-button">
                  {inputFields.map((inputField, index) => (
                    <div
                      className="label-input-button"
                      key={`field-${inputField.id}`}
                    >
                      <label>{inputField.label}</label>
                      <Field
                        type="text"
                        name={inputField.name}
                        placeholder={inputField.place}
                        // onChange={(event) =>
                        //   this.handleChange(inputField.name, event)
                        // }
                      />
                      <ErrorMessage component="div" name={inputField.name} className="error-style" />
                    </div>
                  ))}
                  <div className="submit-form">
                    <button type="submit">ثبت اطلاعات</button>
                    <button type="button" onClick={this.hideForm}>
                      بستن
                    </button>
                  </div>
                </Form>
               )}
              </Formik>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const inputFields = [
  {
    id: 1,
    label: "نام",
    name: "name",
    place: "نام خود را وارد کنید",
    value: "",
  },
  {
    id: 2,
    name: "lastName",
    label: "نام خانوادگی",
    place: "نام خانوادگی خود را وارد کنید",
    value: "",
  },
  {
    id: 3,
    name: "mobile",
    label: "شماره همراه",
    place: "شماره همراه خود را وارد کنید",
    value: "",
  },
  {
    id: 4,
    name: "email",
    label: "پست الکترونیکی",
    place: "پست الکترونیکی خود را وارد کنید",
    value: "",
  },
];
