import React from 'react';
import './formCreate.css';

export default class Form extends React.Component {
  constructor(props) {
    super(props);
    this.showForm = this.showForm.bind(this);
  }

  showForm = () => {
    this.props.onClick();
    console.log("click");
  }

  render() {
    return (
      <div className="create-form">
        <div className="header-form">
          <div className="title-form">
            <div className="content-form">
              مدیریت کاربران
            </div>
            <div className="path">
              داشبورد
            </div>
          </div>

          <div className="add-form">
            <button type="menu" onClick={this.showForm}><i>+</i><span>افزودن کاربر</span></button>
          </div>
        </div>

        <div className="form-container">
          <div className="header-form-container">
            <i>+</i><span>جستجو کاربر</span>
          </div>
          <div className="inputs-form">
            <form>
            {inputFields.map((inputField, index) => 
             <div className="label-input">
                <label>{inputField.label}</label>
                <input type="text" placeholder={inputField.place} />
              </div>
            )}
            </form>
          </div>

          {buttons.map((button, index) => 
            <div className="buttons-form">
              <button type="button"><i className={button.icon}></i><span>{button.name}</span></button>
            </div>
          )}
        </div>

        <div className="footer-form">
          <button type="button"><i className="fa fa-file-excel-o"></i><span>خروجی اکسل</span></button>
        </div>
      </div>

    );
  }
}

const inputFields = [
  {
    label: "نام",
    place: "نام خود را وارد کنید"
  },
  {
    label: "نام خانوادگی",
    place: "نام خانوادگی خود را وارد کنید"
  },
  {
    label: "شماره همراه",
    place: "شماره همراه خود را وارد کنید"
  },
  {
    label: "پست الکترونیکی",
    place: "پست الکترونیکی خود را وارد کنید"
  },
]

const buttons = [
  {
    icon: "fa fa-search",
    name: "جستجو"
  },
  {
    icon: "fa fa-close",
    name: "لغو فیلتر"
  }
]