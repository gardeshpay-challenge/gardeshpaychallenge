import React from 'react';
import './header.css';

export default class Header extends React.Component {
  render() {
    return (
      <div className="header">
        <div className="title-panel">
          <h3>پنل مشتریان - شرکت چنلید</h3>
        </div>
        <div className="profile-panel">
          <i className="fa fa-user"></i>
          <span>112,500,000 ریال | امیر رستمی</span>
          <span>arrow</span>
        </div>
        <div className="exit-logo">
          <i className="fa fa-sign-out"></i>
          <span>خروج</span>
        </div>
      </div>
    );
  }
}