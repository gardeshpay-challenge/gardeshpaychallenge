import React from 'react';
import './section.css';

export default class Section extends React.Component {
  render() {
    console.log(this.props.data);
    return (
      <section>
        <table>
          <thead>
          <tr>
          {tableHeader.map((th, index) => 
              <th>{th.header}<i className="fa fa-sort"></i></th>
          )}
          </tr>
          </thead>

          <tbody>
            <tr>
            {
              Object.keys(this.props.data).map((key, index) => 
              <td key={`field-${index}`}>
                {this.props.data[key]}
              </td>)
            }
            </tr>
          </tbody>
          </table>
      </section>
    );
  }
}

const tableHeader = [
  {
    id: 2,
    name: "name",
    header: "نام"
  },
  {
    id: 3,
    name: "lastName",
    header: "نام خانوادگی"
  },
  {
    id: 4,
    name: "mobile",
    header: "شماره همراه"
  },
  {
    id: 5,
    name: "email",
    header: "پست الکترونیکی"
  },
]